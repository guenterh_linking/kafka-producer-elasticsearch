import org.apache.logging.log4j.LogManager
import org.junit.Test
import org.swissbib.linked.App
import org.swissbib.linked.ElasticIndex
import org.swissbib.linked.PropertiesLoader
import java.util.*

/*
 * elasticsearch producer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class AppTest {
    private val log = LogManager.getLogger("elastic-producer")

    @Test
    fun testProducer() {
        val propertiesLoader = PropertiesLoader(log)
        ElasticIndex(propertiesLoader, log).scroll()
    }
}