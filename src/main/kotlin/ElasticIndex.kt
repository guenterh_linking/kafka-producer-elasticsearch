/*
 * elasticsearch producer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.http.HttpHost
import org.apache.logging.log4j.Logger
import org.elasticsearch.action.search.ClearScrollRequest
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchScrollRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indices.GetIndexRequest
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.index.query.WrapperQueryBuilder
import org.elasticsearch.search.Scroll
import org.elasticsearch.search.SearchHit
import org.elasticsearch.search.builder.SearchSourceBuilder
import java.io.PrintWriter
import java.io.StringWriter
import java.net.ConnectException
import kotlin.system.exitProcess

class ElasticIndex(
    properties: PropertiesLoader,
    private val log: Logger) {

    private val appProperties = properties.appProperties

    private val producer = Producer(properties, appProperties.getProperty("kafka.topic"), log)
    private val elastic = connect()
    private fun connect(): RestHighLevelClient {
        return RestHighLevelClient(
            RestClient.builder(
                HttpHost(appProperties.getProperty("elastic.host"), appProperties.getProperty("elastic.port").toInt())
            )
        )
    }

    private val index = appProperties.getProperty("elastic.index")!!
    init {
        try {
            if (elastic.indices().exists(GetIndexRequest(index), RequestOptions.DEFAULT)) {
                log.info("Messages are sent to index {}.", index)
            } else {
                log.error("Target index does not exist {}. Shutdown service ...", index)
                exitProcess(1)
            }
        } catch (ex: ConnectException) {
            log.error("Could not connect to elasticsearch at {}:{} because of {}.",
                appProperties.getProperty("elastic.host"),
                appProperties.getProperty("elastic.port"), ex.message)
            exitProcess(1)
        }
    }

    private val query = appProperties.getProperty("elastic.query")!!
    private val include = appProperties.getProperty("elastic.include")?.split(',')?.toTypedArray()
    private val exclude = appProperties.getProperty("elastic.exclude")?.split(',')?.toTypedArray()

    private var exception = false

    fun scroll() {
        log.info("Begin scrolling the index \"$index\".")
        var count = 0
        val scroll = Scroll(TimeValue.timeValueMinutes(1L))
        val searchSourceBuilder = SearchSourceBuilder()
            .query(WrapperQueryBuilder(query))
            .size(5000)
            .fetchSource(include, exclude)
        val searchRequest = SearchRequest(index)
            .source(searchSourceBuilder)
            .scroll(scroll)

        var scrollId: String? = null
        var totalDuration = 0L
        try {
            var response = elastic.search(searchRequest, RequestOptions.DEFAULT)
            scrollId = response.scrollId!!
            var hits: Array<SearchHit>? = response?.hits?.hits

            log.info("Document count in scroll: " + response.hits.totalHits)
            while (hits != null && hits.isNotEmpty()) {
                val start = System.nanoTime()

                for (hit in hits) {
                    producer.send(hit.id, hit.sourceAsString)
                }
                val duration = System.nanoTime() - start
                totalDuration += duration
                val durationSeconds: Double = duration / 1000000000.0
                count += hits.size
                log.info("Processed ${hits.size} documents in $durationSeconds seconds.")
                val scrollRequest = SearchScrollRequest(scrollId)
                scrollRequest.scroll(scroll)

                response = elastic.scroll(scrollRequest, RequestOptions.DEFAULT)
                scrollId = response.scrollId
                hits = response.hits.hits
            }
        } catch (ex: Exception) {
            val string = StringWriter()
            val print = PrintWriter(string)
            ex.printStackTrace(print)
            log.error(string.toString())
            exception = true
        } finally {
            producer.close()
            if (scrollId != null) {
                val clearScrollRequest = ClearScrollRequest()
                clearScrollRequest.addScrollId(scrollId)
                elastic.clearScroll(clearScrollRequest, RequestOptions.DEFAULT)
                elastic.close()
            }
            log.info("Processed a total of $count messages in ${totalDuration / 1000000000.0} seconds.")
            if (exception)
                exitProcess(1)
            else
                exitProcess(0)
        }
    }
}