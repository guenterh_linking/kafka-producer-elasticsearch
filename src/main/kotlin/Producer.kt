/*
 * elasticsearch producer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsMergeActions
import java.util.*

class Producer(private val properties: PropertiesLoader, private val topic: String, private val log: Logger) {
    private val instance = KafkaProducer<String, SbMetadataModel>(properties.kafkaProperties)
    private val model = SbMetadataModel().setEsMergeAction(getEsMergeAction())
    private val exportDate = properties.appProperties.getProperty("export.date")

    init {
        if (exportDate != null) {
            model.messageDate = exportDate
        }
    }
    private fun getEsMergeAction(): EsMergeActions {
        return when (properties.appProperties.getProperty("kafka.status")) {
            "UPDATE" -> EsMergeActions.UPDATE
            else -> EsMergeActions.NEW
        }
    }

    fun close() {
        instance.close()
    }

    fun send(key: String, message: String) {
        instance.send(ProducerRecord<String, SbMetadataModel>(topic, key, model
            .setData(message)
            .setEsIndexName(properties.appProperties.getProperty("elastic.index"))))
        log.debug("Sent message {} to topic {}.", message, topic)
    }
}