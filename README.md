# Kafka Producer Elasticsearch

A small application which can be configured with an app.properties file. It scrolls an
elasticsearch index with the specified query and sends each returned document as a
message wrapped in the swissbib Metadata Wrapper.

## Configuration

### elastic.host (mandatory)

The elasticsearch host name.

### elastic.port (mandatory)

The elasticsearch port number.

### elastic.index (mandatory)

The elasticsearch index name.

### elastic.include (optional)

A comma separated list of fields to include. Can be omitted or left empty
to include all fields. As soon as a field is given only those fields
will be included in the source. Use dot-notation to target specific subfields
only. Use a star (*) to include all subfields of an object.

### elastic.exclude (optional)

A comma separated list of fields to exclude. When omitted or left empty 
no fields are excluded. Use dot-notation to exclude specific subfields and 
a star (*) to exclude all subfields.

### elastic.query (optional)

The query with which to scroll the index. When no query is given the default 
is to match all results. The query needs to be valid json (no escape characters necessary).
Only add the json within the outer most query definition

### application.id (mandatory)

Must be set to a unique value between all kafka streams applications.

### bootstrap.servers (mandatory)

A comma separated list of kafka broker host:port definitions. Requires at
least one host to be defined. 

### kafka.topic (mandatory)

The topic the messages are published to.

### kafka.status (optional)

Can be either `UPDATE` or `NEW`. Default is `NEW`. Is used in the workflow
to input update cluster ids which determines whether the message should 
be indexed as a new item or only merged into existing documents.

## Logging

All logging output goes to standard out. Which is then collected by the Docker logging and can be read from docker logs.
